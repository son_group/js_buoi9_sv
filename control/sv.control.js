function getDocumentID(id){
    return document.getElementById(id);
}

function getStudentInfoFromForm(){
    var studentID = getDocumentID("txtMaSV").value;
    var studentName = getDocumentID("txtTenSV").value;
    var email = getDocumentID("txtEmail").value;
    var password = getDocumentID("txtPass").value;
    var math = getDocumentID("txtDiemToan").value;
    var physical = getDocumentID("txtDiemLy").value;
    var chemistry = getDocumentID("txtDiemHoa").value;
    
    return new Student(studentID,studentName,email,password,math,physical,chemistry);
    
}

function renderStudent(studentList){
    console.log("studentList ",studentList);
    var contentHTML = "";
    for(var index=0;index<studentList.length;index++){
        var student = studentList[index];
        // console.log("studentID of render",student);
        var rowHTML = `<tr>
            <td>${student.studentID}</td>
            <td>${student.studentName}</td>
            <td>${student.email}</td>
            <td>${student.averagePoint()}</td>
            <td>
                <button onclick = "deleteStudent('${student.studentID}')" class="btn btn-danger">Xoá</button>
                <button onclick ="editStudent('${student.studentID}')" class="btn btn-warning">Sửa</button>
            </td>
            </tr>
        `;        
        contentHTML += rowHTML;
    }
    getDocumentID("tbodySinhVien").innerHTML = contentHTML;
}

function findIndex(studentID, studentList){
    // console.log("studentID",studentID);
    for (var index = 0; index<studentList.length; index++){
        var student = studentList[index];
        
        if(studentID == student.studentID){
            // console.log("gia tri index trong find Index ",index);
            return index;
        }
    }
    return -1;
}

function showStudentInforToForm(student){
    getDocumentID("txtMaSV").value = student.studentID;
    getDocumentID("txtTenSV").value = student.studentName;
    getDocumentID("txtEmail").value = student.email;
    getDocumentID("txtPass").value = student.password;
    getDocumentID("txtDiemToan").value = student.math;
    getDocumentID("txtDiemLy").value = student.physical;
    getDocumentID("txtDiemHoa").value = student.chemistry;
    getDocumentID("txtMaSV").disabled = true;
    getDocumentID("addStudent").disabled = true;

    console.log("show id len form: ",student.studentID);
}

function clearForm(){
    getDocumentID("txtMaSV").value = "";
    getDocumentID("txtTenSV").value = "";
    getDocumentID("txtEmail").value = "";
    getDocumentID("txtPass").value = "";
    getDocumentID("txtDiemToan").value = "";
    getDocumentID("txtDiemLy").value = "";
    getDocumentID("txtDiemHoa").value = "";
    getDocumentID("txtMaSV").disabled = false;
    getDocumentID("updateStudentInfo").disabled = true;
    getDocumentID("addStudent").disabled = false;

    
}
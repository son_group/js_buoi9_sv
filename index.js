const LOCALSTORAGE_STUDENTLIST = "LOCALSTORAGE_STUDENTLIST";
var studentList = [];

var studentListJson = localStorage.getItem(LOCALSTORAGE_STUDENTLIST);
if (studentListJson != null) {
  studentList = JSON.parse(studentListJson);
  for (var index = 0; index < studentList.length; index++) {
    var student = studentList[index];
    studentList[index] = new Student(
      student.studentID,
      student.studentName,
      student.email,
      student.password,
      student.math,
      student.physical,
      student.chemistry
    );
  }
  renderStudent(studentList);
}
clearForm();

function themSV() {
  var student = getStudentInfoFromForm();
  var isValid =
    validation.checkEmptyInput(
      student.studentID,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.checkLength(
      student.studentID,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );

  // console.log("isValid after check ID",isValid);

  isValid =
    isValid &
    validation.checkEmptyInput(
      student.studentName,
      "spanTenSV",
      "Tên sinh viên không được để trống"
    );

  isValid =
    isValid &
      (validation.checkEmptyInput(
        student.email,
        "spanEmailSV",
        "Email không được để trống"
      ) &&
    validation.checkEmailValid(
      student.email,
      "spanEmailSV",
      "Email không hợp lệ"
    ));

  isValid =
    isValid &
    validation.checkEmptyInput(
      student.password,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    );

  isValid =
    isValid &
      (validation.checkEmptyInput(
        student.math,
        "spanToan",
        "Điểm Toán không được để trống"
      ) &&
    validation.checkPointValid(
      student.math,
      "spanToan",
      "Điểm Toán không được nhỏ hơn 0 hoặc lớn hơn 10",
      0,
      10
    ));

  isValid =
    isValid &
      (validation.checkEmptyInput(
        student.physical,
        "spanLy",
        "Điểm Lý không được để trống"
      ) &&
    validation.checkPointValid(
      student.physical,
      "spanLy",
      "Điểm Lý không được nhỏ hơn 0 hoặc lớn hơn 10",
      0,
      10
    ));

  isValid =
    isValid &
      (validation.checkEmptyInput(
        student.chemistry,
        "spanHoa",
        "Điểm Hoá không được để trống"
      ) &&
    validation.checkPointValid(
      student.chemistry,
      "spanHoa",
      "Điểm Hoá không được nhỏ hơn 0 hoặc lớn hơn 10",
      0,
      10
    ));

    isUniqueStudent = validateStudentID.checkUnique(student.studentID,studentList);
    console.log("Unique StudentID ",isUniqueStudent);

  if (isValid && isUniqueStudent) {
    studentList.push(student);
    var studentListJson = JSON.stringify(studentList);
    localStorage.setItem(LOCALSTORAGE_STUDENTLIST, studentListJson);
    renderStudent(studentList);
    clearForm();
  }
  // console.log("Student of themSV getfromform ",student);
}

function editStudent(studentID) {
  var index = findIndex(studentID, studentList);
  console.log("index cua edit", index);
  console.log("gia tri index trong file index.js ", index);
  if (index != -1) {
    var student = studentList[index];
    showStudentInforToForm(student);
  }
  document.getElementById("updateStudentInfo").disabled = false;
}

function updateStudent() {
  var student = getStudentInfoFromForm();
  var index = findIndex(student.studentID, studentList);
  studentList[index] = student;
  localStorage.removeItem(studentList);
  var studentListJson = JSON.stringify(studentList);
  localStorage.setItem(LOCALSTORAGE_STUDENTLIST, studentListJson);
  renderStudent(studentList);
  clearForm();
}

function deleteStudent(studentID) {
  var index = findIndex(studentID, studentList);
  studentList.splice(index, 1);
  var studentListJson = JSON.stringify(studentList);
  localStorage.setItem(LOCALSTORAGE_STUDENTLIST, studentListJson);
  renderStudent(studentList);
}

function resetForm() {
  clearForm();
}

function searchStudent() {
  var studentName = document.getElementById("txtSearch").value;
  if (studentName == null || studentName == "") {
    renderStudent(studentList);
  } else {
    var studentListSearch = [];
    studentList.forEach((student) => {
      if (
        validateString.checkStringIncludeStringFind(
          student.studentName,
          studentName
        )
      ) {
        studentListSearch.push(student);
      }
    });
    if (studentListSearch.length == 0) {
      getDocumentID("tbodySinhVien").innerHTML = `<tr>
      <td>Không có sinh viên nào có tên như vậy</td></tr>`;
    }else{
        renderStudent(studentListSearch);
    }
  }
}

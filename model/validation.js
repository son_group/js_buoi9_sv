var validation = {
    checkEmptyInput: function(value, idError, message){
        if(value.length ==0){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkLength: function(value, idError, message, minlength, maxlength){
        if(value.length <minlength || value.length>maxlength){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkEmailValid: function(value,idError,message){
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(value.toLowerCase().match(re)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkPointValid: function(value, idError,message,minPoint,maxPoint){
        if(value <minPoint || value>maxPoint){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    }
}

var validateString = {
    checkStringIncludeStringFind: function(stringValue, subStringValue){
        if(stringValue.toLowerCase().includes(subStringValue.toLowerCase())){
            return true;
        }else{
            return false;
        }        
    }
}

var validateStudentID = {
    checkUnique: function(studentID, studentList){
        var check = true ;
        
       for(var index=0;index<studentList.length;index++){
           var student = studentList[index];
            if(student.studentID.toLowerCase() ==studentID.toLowerCase()){                
                check = false;
                index = studentList.length;                
            }
        };
        
        if(check==true){            
            document.getElementById("spanMaSV").innerText = "";
        }else{            
            document.getElementById("spanMaSV").innerText = "Mã sinh viên bị trùng";
        }
        return check;
    }
}
function Student(studentID, studentName, email, password, math, physical, chemistry){
    this.studentID = studentID;
    this.studentName = studentName;
    this.email = email;
    this.password = password;
    this.math = math;
    this.physical = physical;
    this.chemistry = chemistry;
    this.averagePoint = function(){
        return (this.math*1 + this.physical*1 + this.chemistry*1)/3;
    };
}